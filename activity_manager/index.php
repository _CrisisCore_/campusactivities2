<?php

if(!isset($_SESSION)) 
{ 
    session_start(); 
} 

//print_r($_SESSION);

//error_reporting(E_ALL);
//ini_set('display_errors',1);
//sleep(int seconds); delays the script execution for a number of seconds
//Warning: $action should always be NULL on first visit of index.php
//DEBUG
//echo $action;
//ini_set("session.gc_maxlifetime","1440");

require_once('../model/database.php');
require_once('../model/admin_db.php');
require_once('../model/clubs_db.php');
require_once('../model/majors_db.php');
//require_once('../model/Clockwork.php');
require_once('../model/activities_db.php');
//for header actions
require_once('../util/valid_guest.php');
require_once('../util/valid_user.php');
//for form actions
//Perform the specified action
//init
$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        if (isset($_SESSION['visited'])) {
             if (!$_SESSION['visited']) {
                echo"<script>alert('You have been logged in. Welcome.')</script>";
             }
        }
        //Never echo again 
        $_SESSION['visited'] = TRUE;
        $action = 'list_activities';
    }
}
switch($action)  {
    case 'list_activities':
        //create a file for this validation for is_valid_user for catalog index
        //constantly validate
        $activities = get_activities(); 
        $activity_id = filter_input(INPUT_GET, 'activity_id', FILTER_VALIDATE_INT);
        $activity_name = get_activity_name($activity_id); 
        //display activity list
        include('activity_list.php');
        break;
    case 'show_add_form':
        //include 
        //add filter input
        $activities = get_activities();
        $clubs = get_clubs();
        $majors = get_majors();
        include('activity_add.php');  
        break;
    case 'show_delete_form':
        //constantly validate
        //add filter input
        $activities = get_activities();
        include('activity_delete.php');  
        break;
    case 'add_activity':
        //Check if this ' ' are correct on the view 
        $name = filter_input(INPUT_POST, 'name');   
        $description = filter_input(INPUT_POST, 'description');
        $date = filter_input(INPUT_POST, 'date');
        $time = filter_input(INPUT_POST, 'time');
        $location = filter_input(INPUT_POST, 'location');
        $category = filter_input(INPUT_POST, 'category');  
        
        if ($category == "Club")
        {
            $category = 1;
        }
        elseif ($category == "Major Activity")
        {
            $category = 3;
        }
        elseif ($category == "Activity")
        {
            $category = 2;
        }
    
        add_activity($name, $description, $date, $time, $location, $category);
        
        
        header('Location: .?action=list_activities'); 
        exit();
        break;
    case 'delete_activity':
        $activity_id = filter_input(INPUT_POST, 'activity_id');    
        
             delete_activity($activity_id);        
    
             header("location: .");
             exit();
        break;
    case 'show_edit_form':
        $activities = get_activities();
        include('activity_edit_list.php');    
        break;
    case 'show_edit_activity': // This redirects activity_edit_list to activity_edit
        $activity_id = filter_input(INPUT_POST, $activity['activity_id']);     
        $activity = get_activity_name($activity_id);
        include('activity_edit.php');    
        break;
    case 'edit_activity':
        $activity_id = filter_input(INPUT_POST, 'activity_id');    
        $name = filter_input(INPUT_POST, 'name');
        $description = filter_input(INPUT_POST, 'description');
        $date = filter_input(INPUT_POST, 'date');
        $location = filter_input(INPUT_POST, 'location');
        $time = filter_input(INPUT_POST, 'time');            
       
        edit_activity($activity_id, $name, $description,$date,$location,$time);
        header('Location: ./?action=list_activities');
        exit();
        break;
    default:
        echo "DEBUG Default location index.php";
        break;
}