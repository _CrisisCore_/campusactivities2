<?php 
require_once('../model/database.php');
require_once('../model/admin_db.php');
require_once('../model/activities_db.php');
require_once('../util/valid_user.php');
require_once('../util/valid_guest.php'); 

if(!isset($category_type)) 
{ 
    $category_type = 0; 
} 
?>

<main>
    <center><h1>Activities</h1> </center>
    <br>

    <body>
       
        <nav>       
            <div class="container" style="width: 90%;">
            <hr class="featurette-divider">
            
            <?php if($category_type == 1) {
            foreach ($activities as $activity) : ?>           
         

		<button class="accordion">
                    <center><h3><?php echo $activity['activity_name']; ?></h3></center>
                </button>
			<div class="panel">
				   <form>
					  <strong>Description:</strong>:<br>
					  <h5> <?php echo $activity['activity_description']; ?></h5><br/>
					  <strong>Meeting Time:</strong><br/>
					  <h5><?php echo $activity['activity_date']; ?> </h5><br/>
					  <strong>Location</strong><br/>
					  <h5><?php echo $activity['location']; ?></h5><br/>
					  <strong>Fee:</strong></br>
					  <h5>$0</h5>
				</form> 
                        </div>
            <?php endforeach; 
            } ?>
               
            <!-- This one is for major events -->
            <?php if ($category_type == 2)  { ?>
                          
               <?php  foreach ($activities as $activity) : ?>  
                    <button class="accordion">
                        <center><h3><?php echo $activity['activity_name']; ?> </h3></center>
                    </button>
			<div class="panel">
                            <form>
				<strong>Description:</strong>:<br>
                                <h5><?php echo $activity['activity_description']; ?></h5><br/>
				<strong>Meeting Time:</strong><br/>
				<h5><?php echo $activity['activity_date']; ?></h5><br/>
				<strong>Location</strong><br/>
				<h5><?php echo $activity['location']; ?></h5><br/>
				<strong>Fee:</strong></br>
				<h5>$0</h5>
				<strong>Majors:</strong></br>
				<h5>Comp Sci, Bio</h5>
				</form> 
			</div>
       
            <?php endforeach; 
            } ?> 
                
            <!-- This one is for clubs -->
            <?php if($category_type == 3) {
                
                    foreach ($activities as $activity) : ?> 
                    <button class="accordion"><center><h3><?php echo $activity['activity_name']; ?></h3></center></button>
                    <div class="panel">
                        <form>  
                            <!-- Placeholder count for now... -->
				<strong>Member Count: 32</strong><br>
				<h5>Members</h5><br>
				<strong>Description:</strong>:<br>
				<h5> <?php echo $activity['activity_description']; ?></h5><br/>
				<strong>Meeting Time:</strong><br/>
				<h5><?php echo $activity['activity_date']; ?></h5></br>
				<strong>Location</strong><br/>
				<h5><?php echo $activity['location']; ?></h5><br/>
				<strong>Member Fee:</strong></br>
				<h5>$45</h5> 
				</form> 
		    </div>
                    <?php endforeach;
                    } ?>
            </div>
        </nav>

    <section>        
        
        <a href="?action=show_add_form" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Add Activity</a><br><br>
        <a href="?action=show_delete_form" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Delete Activity</a><br><br>
        <a href="?action=show_edit_form" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Edit Activity</a>
        
    </section>
        
      <script>
			var acc = document.getElementsByClassName("accordion");
			var i;
			
			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
			    this.classList.toggle("active");
			    var panel = this.nextElementSibling;
			    if (panel.style.maxHeight){
			      panel.style.maxHeight = null;
			    } else {
			      panel.style.maxHeight = panel.scrollHeight + "px";
			    }
			  }
			}
      </script>
     
    </body>
</main>
<?php include '../View/footer.php'; ?>