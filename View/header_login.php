<!DOCTYPE html>
<html lang="en">

<head>
        <!-- jQuery -->
    <script src="../js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>USCB Activities</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/one-page-wonder.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../index.php">USCB <strong>Activities</strong></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav" >
                    <li>
                        <a href= "../index.php" >Home</a>
                    </li>
                    <li>
                        <a href= "../activity_catalog/catalog_index.php?action=list_clubs">Clubs</a>
                    </li>
                    <li>
                        <a href= "../activity_catalog/catalog_index.php?action=list_activities" >Activities</a>
                    </li>
                    <li>
                        <a href= "../activity_catalog/majors.php" >Major Events</a>
                    </li>
                    <li>
                        <a href= "../activity_catalog/contact.php" >Contact</a>
                    </li>
                    <li>
                        <a a href="login_index.php?action=view_login" class="btn btn-lg" role="button" aria-pressed="true">Login</a>
                    </li>  

                    
                    
                    
                </ul>
                
                
                
            </div>
            <!-- /.navbar-collapse -->

          
            
            
        </div>
        <!-- /.container -->
    </nav>
      <!-- Full Width Image Header -->
      <header src="../images/uscb_overhead.jpg" class=" header-image headline nav" alt="uscb_overhead"/> </header>
    <!-- Full Width Image Header -->

