<?php
//Add logic to controller to add a value to is_attending when a button is clicked
//Add logic to controller for action of attend_clicked to get the user_id as well


function add_login($cell_number, $user_name, $user_pass) {
      global $db;
      $user_pass = sha1($user_pass);
      $query = 'INSERT INTO users
                    (cell_number, user_name, user_pass)
                VALUES 
                    (:cell_number, :user_name, :user_pass)';
    $statement = $db->prepare($query);
    $statement->bindValue(':cell_number', $cell_number);
    $statement->bindValue(':user_name', $user_name);
    $statement->bindValue(':user_pass', $user_pass);
    $statement->execute();
    $statement->closeCursor();   
}
//CALL BEFORE ADD USER
//Counts unique cell number in database max poss = 1
//Apply if return value > 0 in controller then user already registered with cell
//Tell user to try again if !valid
function is_valid_cell($cell_number) {
    global $db;
    $query = 'SELECT cell_number FROM users
              WHERE cell_number = :cell_number';
    $statement = $db->prepare($query);
    $statement->bindValue(':cell_number', $cell_number);
    $statement->execute();
    $valid = ($statement->rowCount() == 0);
    $statement->closeCursor();
    //IF !VALID THEN THE USER MUST RE-ENTER cell
    return $valid;
}
//Counts unique user name in database max poss = 1
//Apply if return value  0 in controller then user name already taken
//Use with check if registered
//Tell user to try again if !valid
function is_valid_username($user_name) {
    global $db;
    $query = 'SELECT user_name FROM users
              WHERE user_name = :user_name';
    $statement = $db->prepare($query);
    $statement->bindValue(':user_name', $user_name);
    $statement->execute();
    $valid = ($statement->rowCount() == 0);
    $statement->closeCursor();  
    return $valid;
}

//Just straight up checks if user can login
//Tell user to try again if !valid
function is_valid_user_login($user_name, $user_pass) {
     global $db;
      $user_pass = sha1($user_pass);
      $query = 'SELECT user_id FROM users
                WHERE user_name = :user_name AND user_pass = :user_pass';
    $statement = $db->prepare($query);
    $statement->bindValue(':user_name', $user_name);
    $statement->bindValue(':user_pass', $user_pass);
    $statement->execute();
    $valid = ($statement->rowCount() == 1);
    $statement->closeCursor(); 
    return $valid;
}
//Just straight up checks if user can login
//Tell user to try again if !valid
//use on controller during login
function is_valid_Admin($user_name, $user_pass) {
    global $db;
    $user_pass = sha1($user_pass);
    $query = 'SELECT user_id FROM users
              WHERE user_name = :user_name AND user_pass = :user_pass AND user_name = "admin1337"';
    $statement = $db->prepare($query);
    $statement->bindValue(':user_name', $user_name);
    $statement->bindValue(':user_pass', $user_pass);
    $statement->execute();
    $valid = ($statement->rowCount() == 1);
    $statement->closeCursor(); 
    return $valid;
}
//Warning: move these functions to a different model function file that is protected and only required on admin page
function ban_user($user_name, $user_pass) {
    global $db;
    $user_pass = sha1($user_pass);
    $query = 'DELETE FROM users
              WHERE user_name = :user_name AND user_pass = :user_pass';
    $statement = $db->prepare($query);
    $statement->bindValue(':user_name', $user_name);
    $statement->bindValue(':user_pass', $user_pass);
    $statement->execute();
    $statement->closeCursor(); 
}
function get_user_num($user_name, $user_pass) {
    global $db;
    $user_pass = sha1($user_pass);
    $query = 'SELECT cell_number FROM users
              WHERE user_name = :user_name AND user_pass = :user_pass';
    $statement = $db->prepare($query);
    $statement->bindValue(':user_name', $user_name);
    $statement->bindValue(':user_pass', $user_pass);
    $statement->execute();
    $number = $statement->fetch();
    //convert to string from array
    //may use this in the future to allow multiple messages to user
    //if they have opt in to multiple activity messages
    //basically creates an array of strings - which you can put through a
    //loop for a number of rows that are returned where isAttend is returned 1
    //use an enhanced on the clockwork to iterate through each number and each
    //activity where is attend and user_id are joined
    $number = implode(" ", $number);
    //remove duplicate words in string number that was created by implode
    $number = preg_replace('/(\w{2,})(?=.*?\\1)\W*/', '', $number);
    $statement->closeCursor();
    return $number;  
}