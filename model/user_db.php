<?php
function update_user_attending($cell_number, $clicked_boolean) {
    global $db;
    $query = 'UPDATE users
              SET isAttend = :clicked_boolean
              WHERE cell_number = :cell_number';
    $statement = $db->prepare($query);
    $statement->bindValue(':cell_number', $cell_number);
    $statement->bindValue(':clicked_boolean', $clicked_boolean);
    $statement->execute();
    $statement->closeCursor();   
    return $statement;    
}
//use for later version
//currently unused
function get_nums_activity() {
    global $db;
    $query = 'SELECT cell_number FROM users
              WHERE isAttend = 1';
    $statement = $db->prepare($query);
    $statement->execute();
    $statement->closeCursor();   
    return $numbers;    
}
function get_users() {
    global $db;
    $query = 'SELECT * FROM users
              ORDER BY user_id';            
    $statement = $db->prepare($query);
    $statement->execute();
    $users = $statement->fetchAll();
    $statement->closeCursor();
    return $users;  
}
?>