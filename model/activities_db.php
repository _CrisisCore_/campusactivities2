<?php
function get_activities() {
    global $db;
    $query = 'SELECT * FROM activities
              ORDER BY activity_id';            
    $statement = $db->prepare($query);
    $statement->execute();
    $activities = $statement->fetchAll();
    $statement->closeCursor();
    
    //DEBUG
    //foreach ($activities as $activity) :
    //echo $activity['activity_id'];
    //endforeach;
    return $activities;  
}

//use for clockwork to get sms activity date
function get_activity_date($activity_id) {
    global $db;
    $query = 'SELECT activity_date FROM activities
              WHERE activity_id = :activity_id';            
    $statement = $db->prepare($query);
    $statement->bindValue(':activity_id', $activity_id);
    $statement->execute();
    $activity_date = $statement->fetch();
    $activity_date = implode(" ", $activity_date);
    $activity_date = preg_replace('/(\w{2,})(?=.*?\\1)\W*/', '', $activity_date);
    $statement->closeCursor();
    return $activity_date;
}

//temporary
function get_activity_name2($activity_id) {
    global $db;
    $query = 'SELECT activity_name FROM activities
              WHERE activity_id = :activity_id';    
    $statement = $db->prepare($query);
    $statement->bindValue(':activity_id', $activity_id);
    $statement->execute();    
    $activity = $statement->fetch();
    $statement->closeCursor();    
}
//duplicate function
function get_activity_name($activity_id) {
    global $db;
    $query = 'SELECT activity_name FROM activities
              WHERE activity_id = :activity_id';    
    $statement = $db->prepare($query);
    $statement->bindValue(':activity_id', $activity_id);
    $statement->execute();    
    $activity_name = $statement->fetch();
    $activity_name = implode(" ", $activity_name);
    $activity_name = preg_replace('/(\w{2,})(?=.*?\\1)\W*/', '', $activity_name);
    $statement->closeCursor();   
    return $activity_name;
}
function add_activity($name, $description, $date, $time, $location) {
    global $db;
    $query = 'INSERT INTO activities (activity_name, activity_description, activity_date, activity_time, location)
              VALUES (:name, :description, :date, :time, :location)';
    $statement = $db->prepare($query);
    $statement->bindValue(':name', $name);
    $statement->bindValue(':description', $description);
    $statement->bindValue(':date', $date);
    $statement->bindValue(':time', $time);
    $statement->bindValue(':location', $location);
    $statement->execute();
    $statement->closeCursor();  
}


function delete_activity($activity_id) {
    global $db;
    $query = 'DELETE FROM activities
              WHERE activity_id = :activity_id';
    $statement = $db->prepare($query);
    $statement->bindValue(':activity_id', $activity_id);
    $statement->execute();
    $statement->closeCursor();
}

// HW 03, problem 2
// Add a function that edits categories 
function edit_activity($id, $name, $description) {
    global $db;
    $query = 'UPDATE products
              SET activity_description = :activity_id, 
                    activity_name = :name, 
              WHERE activity_id = :id';
    $statement = $db->prepare($query);
    $statement->bindValue(':id', $id);
    $statement->bindValue(':name', $name);
    $statement->bindValue(':description', $description);
    $statement->execute();
    $statement->fetch();
    $statement->closeCursor();
}

?>