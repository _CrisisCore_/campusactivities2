<?php
function get_clubs() {
    global $db;
    $query = 'SELECT * FROM clubs              
              ORDER BY club_id';
    $statement = $db->prepare($query);
    $statement->execute();
    $clubs = $statement->fetchAll();
    $statement->closeCursor();
    return $clubs;   
}

function get_club_name($club_id) {
    global $db;
    $query = 'SELECT club_name FROM clubs
              WHERE club_id = :club_id';
    $statement = $db->prepare($query);
    $statement->bindValue(':club_id', $club_id);
    $statement->execute();    
    $club_name = $statement->fetch();
    $statement->closeCursor();    
    return $club_name;
}
/*
function get_clubs_old() {
    global $db;
    $query = 'SELECT * FROM activities              
              WHERE club_id = :club_id';
    $statement = $db->prepare($query);
    $statement->execute();
    return $statement;    
}

function get_club_name_old($club_id) {
    global $db;
    $query = 'SELECT * FROM clubs
              INNER JOIN activities
              WHERE activities.club_id = clubs.club_id';
    $statement = $db->prepare($query);
    $statement->bindValue(':activity_id', $activity_id);
    $statement->execute();    
    $activity = $statement->fetch();
    $statement->closeCursor();    
    $club_name = $clubs_arr['club_name'];
    return $activity_name;
}
 * 
 */
?>