<?php 
/*  Author Peter Mize
 * http://www.seleniumhq.org/  //testing reference - firefox addon
*/
//require_once('util/secure_conn.php');
if (!isset($_SESSION)) 
{ 
    session_start(); 
} 
if (isset($_SESSION['is_valid_user']) || isset($_SESSION['is_valid_Admin']))
{
    include('View/header_home2.php');
} else {
    include('View/header_home.php');
}

/* Peter Mize author
//Test to convert array type into string type php
$bruh = array();
print_r($bruh); echo " bruh is" . "\n";
$bruh[0] = "f";
$bruh[1] = "o";
$bruh[2] = "o";
$bruh = implode($bruh);
//you can now use . concatenation property of string types
echo $bruh . " " . " is " . " now " . gettype($bruh). "\n";
*/

//SESSION IS ALWAYS SET 
?>

<body>

    
     <!-- Page Content -->
    <div class="container" style="width: 90%;">

        <hr class="featurette-divider">

        <!-- Admin -->
        <div class="featurette" id="about">
            <img class="featurette-image img-circle img-responsive pull-right" src="images/ice.jpg" style="width:150px;height:150px;">
            <h2 class="featurette-heading"><a href="user_manager/login_index.php">Activity Manager</a>
               <!-- <span class="text-muted">Plenty to keep you occupied</span> -->
            </h2>
            <h3>Manager your activities as a club leader or organizer</h3>
        </div>
        
        <hr class="featurette-divider">
        
        <!-- First Featurette -->
        <div class="featurette" id="about">
            <img class="featurette-image img-circle img-responsive pull-right" src="images/glocade.jpg" style="width:150px;height:150px;">
            <h2 class="featurette-heading"><a href="activity_catalog/catalog_index.php">Activities</a>
               <!-- <span class="text-muted">Plenty to keep you occupied</span> -->
            </h2>
            <h3>Find out what's going on around campus</h3>
        </div>

        <hr class="featurette-divider">

        <!-- Second Featurette -->
        <div class="featurette" id="services">
            <img class="featurette-image img-circle img-responsive pull-right" src="images/gigs.jpg" style="width:150px;height:150px;">
            <h2 class="featurette-heading"><a href="activity_catalog/majors.php">Major Events</a>
                <!--<span class="text-muted">Is Pretty Cool Too.</span> -->
            </h2>
            <p class="lead">Conferences, conventions, and career fairs for specific Majors</p>
        </div>

        <hr class="featurette-divider">

        <!-- Third Featurette -->
        <div class="featurette" id="contact">
            <img class="featurette-image img-circle img-responsive pull-right" src="images/soccer.jpg" style="width:150px;height:150px;">
            <h2 class="featurette-heading"><a href="activity_catalog/catalog_index.php?action=list_clubs">Clubs</a>
               <!-- <span class="text-muted">Will Seal the Deal.</span> -->
            </h2>
            <p class="lead">Find all the available clubs around USCB and search through which ones you may want to join</p>
        </div>

        <hr class="featurette-divider">

        
    </div>
    
</body>

<?php include("View/footer.php"); ?>