<?php
if (!isset($_SESSION)) 
{ 
    session_start(); 
} 
require_once('../model/database.php');
require_once('../model/admin_db.php');
require_once('../model/clubs_db.php');
require_once('../model/majors_db.php');
require_once('../model/user_db.php');
//require_once('../model/Clockwork.php');
require_once('../model/activities_db.php');
require_once('../util/valid_admin.php');
//Perform the specified action
//init

$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        if (isset($_SESSION['visited'])) {
             if (!$_SESSION['visited']) {
                echo"<script>alert('You have been logged in. Welcome Admiral.')</script>";
             }
        }
        //Never echo again only once upon login
        $_SESSION['visited'] = TRUE;
        $action = 'view_admin';
    }
}

switch($action)  {
    case 'view_admin':
        $users = get_users();
        include('admin_page.php');
        exit();
        break;
    //works
    case 'delete_user':
        $username = filter_input(INPUT_POST, 'nameuser');
        $userpass = filter_input(INPUT_POST, 'userpass');
        
        ban_user($username, $userpass); 
        break;
     case 'send_message':
        $username = filter_input(INPUT_POST, 'user_name');
        $userpass = filter_input(INPUT_POST, 'password');
        $activity_id = filter_input(INPUT_POST, 'activityID', FILTER_VALIDATE_INT);
        //temporary only gets 1 activity_name by 1 id may change in future
        $activity_name = get_activity_name($activity_id);
        $number = get_user_num($username, $userpass);
        $activity_date = get_activity_date($activity_id);
        /*
        $_SESSION['activity_name'] = $activity_name;
        $_SESSION['number'] = $number;
        //id admin specifies
        $_SESSION['activity_id'] = $activity_id;\
         * 
         */
        //in test
        //$_SESSION[$activities] = get_activities();
        
        include_once('../model/Clockwork.php');
        exit();
        break;
}
