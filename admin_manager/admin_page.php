<?php // Placeholder for an admin page 
//Warning: security risk if print_r is used!
/* http://www.seleniumhq.org/ */ //testing reference

include('../View/header_admin.php');
?>

<main>
    <h1>Manage User Permissions</h1>
    <div>           
        <form action="./admin_index.php" method="post" id="user_perm_form">
            <input type="hidden" name="action" value="delete_user">           

            <p> Delete a user; i.e remove their current access & authorization. </p>
            
            <label>Username <strong>of User</strong> you want to Delete:</label>
            <input type="input" name="nameuser" size="50" value="nameuser" />
            <br>
            <label>Password of User you want to Delete:</label>
            <input type="input" name="userpass" size="50" value="userpass" />
            <br>
            <label>&nbsp;</label>
            <input type="submit" value="Delete" />
            <br>
        
        </form>  
    
    </div>
    
    <h1>Send User Message</h1>
    <div>
        <form action="./admin_index.php" method="post" id="mess_send_form">
            <input type="hidden" name="action" value="send_message">
            
            <p> Send a message to a user to remind them of an activity. </p>
            
            <label>Username of User to send message to:</label>
            <input type="input" name="user_name" size="25" value="user_name" />
            <br>
            <label>Password of User to send message to:</label>
            <input type="input" name="password" size="25" value="password" />
            <br>
            <label>Activity ID to send to User:</label>
            <input type="input" name="activityID" size="25" value="activityID" />
            <br>
        
            <label>&nbsp;</label>
            <input type="submit" value="Send Message" />
        </form>
    </div>
        