<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
} 
require('../model/database.php');
require('../model/activities_db.php');
require('../model/clubs_db.php');
require_once('../util/valid_guest.php');
// require_once('../model/Clockwork.php');
//Default display activities for now

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * 
 * CONTROLLER FOR CATALOG
 */
$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'list_activities';
    }
}
switch($action)  {
    case 'list_activities':
        //create a file for this validation for is_valid_user for catalog index
        //constantly validate
        $activities = get_activities(); 
        $activity_id = filter_input(INPUT_GET, 'activity_id', FILTER_VALIDATE_INT);
        $activity_name = get_activity_name($activity_id); 
        //display activity list
        include('activities.php');
        exit();
        break;
    case 'list_clubs':
        $clubs = get_clubs(); 
        $club_id = filter_input(INPUT_GET, 'club_id', FILTER_VALIDATE_INT);
        $activity_name = get_club_name($club_id); 
        include('clubs.php');
        exit();
        break;
}
