<?php 
require_once('../model/database.php');
require_once('../model/admin_db.php');
require_once('../model/clubs_db.php');
require_once('../util/valid_guest.php');
?>

<body>

    <!-- Page Content -->
    <div class="container" style="width: 90%;">

        <hr class="featurette-divider">

		
					
            <?php 
            foreach ($clubs as $club) : ?> 
                        <button class="accordion"><center><h3><?php echo $club['club_name']; ?></h3></center></button>
			<div class="panel">
                            <form>          
                                 
                                 <center><strong><h3>Description:</h3></strong></center>
                                <center> <h4> <?php echo $club['club_description']; ?> </h4> </center><br>
                                <center> <strong><h3>Meeting Time:</h3></strong></center>
                                <center> <h4><?php echo $club['meeting_time'];?> </h4> </center><br>
                                <center> <strong><h3>Location:</h3></strong></center>
                                <center> <h4> <?php echo $club['location']; ?> </h4> </center>       
                                
                               
                                
                            </form> 
                            
			</div>
			
            <?php endforeach; ?>
			
		
		
		<script>
			var acc = document.getElementsByClassName("accordion");
			var i;
			
			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
			    this.classList.toggle("active");
			    var panel = this.nextElementSibling;
			    if (panel.style.maxHeight){
			      panel.style.maxHeight = null;
			    } else {
			      panel.style.maxHeight = panel.scrollHeight + "px";
			    }
			  }
			}
		</script>
	        
    </div>
</body>

</div>
 <!-- Full Width Image Header -->
    <header class="header-text">
        <!-- <img src="images/uscb_overhead.jpg" alt="uscb_overhead" /> -->
    </header>
</body>
<?php include("../View/footer.php");
