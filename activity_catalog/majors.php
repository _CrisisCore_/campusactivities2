 <?php 

require_once('../model/database.php');
require_once('../model/admin_db.php');
require_once('../model/majors_db.php');
require_once('../util/valid_guest.php');
$activities = get_majors();  
?>

<body>

    <!-- Page Content -->
    <div class="container" style="width: 90%;">

        <hr class="featurette-divider">

		
					
            <?php 
            foreach ($activities as $activity) : ?> 
                        <button class="accordion"><center><h3><?php echo $activity['activity_name']; ?></h3></center></button>
			<div class="panel">
                            <form>          
                                 
                                 <center><strong><h3>Description:</h3></strong></center>
                                <center> <h4> <?php echo $activity['activity_description']; ?> </h4> </center><br>
                                <center> <strong><h3>Meeting Time:</h3></strong></center>
                                <center> <h4> <?php echo $activity['activity_date'];?> &nbsp; <?php echo $activity['activity_time'];?> </h4> </center><br>
                                <center> <strong><h3>Location:</h3></strong></center>
                                <center> <h4> <?php echo $activity['location']; ?> </h4> </center>                                
                                
                            </form> 
			</div>
			
            <?php endforeach; ?>
			
		
		
		<script>
			var acc = document.getElementsByClassName("accordion");
			var i;
			
			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
			    this.classList.toggle("active");
			    var panel = this.nextElementSibling;
			    if (panel.style.maxHeight){
			      panel.style.maxHeight = null;
			    } else {
			      panel.style.maxHeight = panel.scrollHeight + "px";
			    }
			  }
			}
		</script>
	        
    </div>
</body>
<?php include("../View/footer.php"); ?>