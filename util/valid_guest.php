<?php
if(!isset($_SESSION)) 
{ 
    session_start();
} 
//DEBUG
//print_r($_SESSION);
if (!isset($_SESSION['is_guest']) || !isset($_SESSION['is_valid_user']) || !isset($_SESSION['is_valid_Admin'])) {
    $_SESSION['is_guest'] = TRUE;
}


if (($_SESSION['is_guest']) && !isset($_SESSION['is_valid_user']) && !isset($_SESSION['is_valid_Admin'])) {
        include('../View/header_guest.php');
} else if ($_SESSION['is_guest'] && $_SESSION['is_valid_user'] && !isset($_SESSION['is_valid_Admin']))  {
        include('../View/header.php');
} else if ($_SESSION['is_guest'] && $_SESSION['is_valid_user'] && $_SESSION['is_valid_Admin']) {
        include('../View/header_admin.php');
}
