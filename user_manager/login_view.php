<?php    
include("../View/header_login.php"); 
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<body>
    <div class="container" style="width: 90%;">
        <form action="login_index.php" method="post" id="login_form">
                    <input type='hidden' name='action' value='login'>
			 
			  <div class="container">
			    <label><b>Username</b></label>
			    <input type="text" placeholder="Enter Username" name="user_name" required>
			
			    <label><b>Password</b></label>
			    <input type="password" placeholder="Enter Password" name="user_pass" required>
			    
				
			    <button class="page" name="login" value="login" type="submit">Login</button>
			    
			      <!-- Needs future work -->
			    <input type="checkbox" checked="checked"> Remember me
			  </div>
			
			  <div class="container" style="background-color:#f1f1f1">
			    
			    <!-- <span class="psw">Forgot <a href="#">password?</a></span> -->
			  </div>
		</form>
        <li>
            <a href="login_index.php?action=view_register" class="btn btn-lg" role="button" aria-pressed="true"><strong>REGISTER HERE</strong></a>
        </li>
    </div>
</body>

<?php
include("../View/footer.php");
?>