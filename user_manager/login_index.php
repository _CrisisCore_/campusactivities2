<?php
if (!isset($_SESSION)) 
{ 
    session_start(); 
} 
//get all user ids of active users logged in
//error_reporting(E_ALL);
//NOTICE: ob_start() and ob_end_flush() workaround no longer effective as of
//2012. All browsers do not allow anything else including buffers to run
// while php is running.

//BRANCHER CONTROLLER
require_once('../model/database.php');
require_once('../model/admin_db.php');
//require_once('../model/Clockwork.php');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//init
$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'view_login';
    }
}

switch ($action) {
    case 'view_login':
        include('login_view.php');
        break;
    case 'login':
        $user_name=filter_input(INPUT_POST, 'user_name');//here getting result from the post array after submitting the form.  
        $user_pass=filter_input(INPUT_POST, 'user_pass');//same
        //branches admin & user
        if (is_valid_Admin($user_pass) || (is_valid_user_login($user_name, $user_pass) )) {
            if (is_valid_Admin($user_name, $user_pass)) {
                  $_SESSION['is_valid_Admin'] = TRUE;
                  $_SESSION['is_valid_user'] = TRUE;
                  $_SESSION['is_guest'] = TRUE;
                  $_SESSION['visited'] = FALSE;
            } 
            if (is_valid_Admin($user_name, $user_pass)) {
                  header('Location: ../admin_manager/admin_index.php'); 
                  exit();
                  break;
            }
            //working
            if (is_valid_user_login($user_name, $user_pass)) {
                //print_r($_SESSION);
                //echo"<script>alert('You have been logged in. Welcome User.')</script>";
                $_SESSION['is_valid_user'] = TRUE;
                $_SESSION['is_guest'] = TRUE;
                $_SESSION['visited'] = FALSE;
            }
            if (is_valid_user_login($user_name, $user_pass)) {
                
                
                //print_r($_SESSION);
                //echo"<script>alert('You have been logged in. Welcome User.')</script>";
                
                header('Location: ../activity_manager/index.php'); 
                exit();
                break;
            }
        }
        //branch guest
        //working
        if (!is_valid_user_login($user_name, $user_pass) && !is_valid_Admin($user_name, $user_pass)) {
            //start guest session
            $_SESSION['is_guest'] = TRUE;
            //print_r($_SESSION);
            $error_message = "Invalid Login. Please try again.";
            $_SESSION['invalid_login'] = TRUE;
            
            include_once('../errors/error.php');
            exit();
            break;
        } 
        break;
    case 'view_register':
        include('register_view.php');
        break;
    case 'register':
        $user_name=filter_input(INPUT_POST, 'user_name');//here getting result from the post array after submitting the form.  
        $user_pass=filter_input(INPUT_POST, 'user_pass');//same  
        $cell_number=filter_input(INPUT_POST, 'cell_number');//same
        
        //create a validate for this
        $phone = preg_replace('/[^0-9]/', '', $cell_number);
        //Checks if the length is not 10 numbers 
        if(!(strlen($phone) === 10)) {
        //Phone is 10 characters in length (###) ###-#### no hyphens
        //preg_replace will fix any unneccessary characters
            $error_message = 'A cell number must be 10 characters no hyphens i.e. (1234567890)';   
            include('../errors/error.php');
        } else if(!(is_valid_cell($phone))) {  
            $error_message = 'Please re-enter the cell number this has already been registered.';   
            include('../errors/error.php');
        }else if(!(is_valid_username($user_name))) {
            $error_message = 'Invalid; One or more of these is taken. Please go back and re-enter the username or password';
            include('../errors/error.php');
        } else {
            //Is function doing sha1?? 
            add_login($phone, $user_name, $user_pass);
            // change this to a different action and provide auto fill login later after registration
            //echo"<script>alert('You have been Registered. You are automatically logged in.')</script>";
            //verify login after registration and auto sign in :D! Convenient!
            //if (is_valid_Admin($user_pass) || is_valid_user_login($user_name, $user_pass)) {
            //}
            if (is_valid_Admin($user_name, $user_pass)) {
                $_SESSION['is_valid_admin'] = TRUE;
                $_SESSION['is_valid_user'] = TRUE;
                $_SESSION['is_guest'] = TRUE;
                $_SESSION['visited'] = FALSE;
                header('Location: ../admin_manager/admin_index.php?action=view_admin'); 
                exit();
                break;
            }
            if (is_valid_user_login($user_name, $user_pass)) {
                $_SESSION['is_valid_user'] = TRUE;
                $_SESSION['is_guest'] = TRUE;
                $_SESSION['visited'] = FALSE;
                //include or header?
                header('Location: ../activity_manager/index.php'); 
                exit();
                break;
            } else {
                $_SESSION['is_valid_guest'] = TRUE;
                $error_message = "Uh Oh! Something's broken...If you reach this page call the Coast Guard!";
                include('../errors/error.php');
                break;
            }
        } 
        break;
    case 'logout':   
        //???NOT REACHING THIS
        $_SESSION = array(); // Clear all session data from memory
        session_destroy();   // Clean up the session ID
        echo "<script>alert('You have been successfully logged out!')</script>";
        header('Location: ../index.php');
        exit();
        //Preventative measure if someone navigates to page without session start
        break;
}